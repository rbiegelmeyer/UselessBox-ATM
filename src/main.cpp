#include <Arduino.h>
#include <Servo.h>

Servo servo;
Servo servo2;
int cont = 0;

void setup()
{
    pinMode(13, INPUT_PULLUP);
    servo.attach(9);
    servo2.attach(10);
    servo.write(200);
    delay(1000);

    servo2.write(100);
    delay(2000);
    servo2.write(50);
    delay(1000);
    
}

void movimentos(int mov)
{
    if (mov < 3)
    {
        for (int i = 200; i > 30; i--)
        {
            servo.write(i);
            delay(10);
        }
        for (int i = 30; i < 200; i++)
        {
            servo.write(i);
            delay(10);
        }
    }
    else if (mov < 6)
    {
        for (int i = 200; i > 30; i--)
        {
            servo.write(i);
            delay(5);
        }
        for (int i = 30; i < 200; i++)
        {
            servo.write(i);
            delay(5);
        }
    }
    else if (mov < 8)
    {
        for (int i = 200; i > 70; i--)
        {
            servo.write(i);
            delay(4);
        }
        delay(3000);
        servo.write(30);
        while (!digitalRead(13))
            ;
        servo.write(200);
    }
    else if (mov < 20)
    {
        delay(100);
        servo.write(30);
        while (!digitalRead(13))
            ;
        servo.write(200);
    }
    cont++;
}

void loop()
{
    if (!digitalRead(13))
    {
        movimentos(cont);
    }
}